# TP2 - Calculatrice 
## Features

- [x]  Nombres négatifs
- [x]  Affichage portrait / paysage (avec constraintlayout)
- [x]  Remise à zéro
- [x]  Gère la division par 0
- [x]  Bouton pour quitter l'application
- [x]  Image de l'application modifiée
- [x]  Utilisation du layout linéaire vertical

![vertical](vertical.png)

![landscape](landscape.png)
