package com.example.calcroyer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.RadioGroup;
import android.widget.RadioButton
import android.util.Log


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btn_raz: Button? = null
        var btn_equal: Button? = null
        var btn_leave: Button? = null
        var result: TextView? = null

        result = findViewById<View>(R.id.display) as TextView

        btn_raz = findViewById<View>(R.id.button_raz) as Button
        btn_equal = findViewById<View>(R.id.button_equal) as Button
        btn_leave = findViewById<View>(R.id.button_leave) as Button

        var input1: EditText
        var input2: EditText
        var result_value = 0.00;


        input1 = findViewById(R.id.plain_text_input1)
        input2 = findViewById(R.id.plain_text_input2)

        btn_leave.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                finish()
                System.exit(0)
            }
        })

        btn_equal.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                var result_value1 = input1!!.text.toString().toFloat().toDouble()
                var result_value2 = input2!!.text.toString().toFloat().toDouble()

                val rg = findViewById<View>(R.id.radiog) as RadioGroup
                val radiovalue =
                    (findViewById<View>(rg.checkedRadioButtonId) as RadioButton).text.toString()

                Log.d("TAG", radiovalue)

                when (radiovalue) {
                    "Plus" -> {
                        result_value = result_value1 + result_value2;
                    }
                    "Moins" -> {
                        result_value = result_value1 - result_value2
                    }
                    "Multiplié" -> {
                        result_value = result_value1 * result_value2
                    }
                    "Divisé" -> {
                        result_value = result_value1 / result_value2
                    }
                }

                result.setText(result_value.toString())
            }
        })

        btn_raz.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                result.setText((0).toString())
            }
        })

    }
}